#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include "autonomous_vehicle.h"

int main()
{
	//TODO: Scale Factor and map should be read from a map file provided as an
	//      arg.
	double scale_factor = 1.0;
	int map[][8]= {{0, 0, 0, 0, 0, 0, 0, 0}, 
	               {0, 0, 0, 0, 0, 0, 0, 0},
	               {0, 0, 1, 1, 1, 1, 0, 0},
	               {0, 0, 1, 1, 1, 1, 0, 0},
	               {0, 0, 1, 1, 1, 1, 0, 0},
	               {0, 0, 0, 0, 0, 0, 0, 0},
	               {0, 0, 0, 0, 0, 0, 0, 0}};

	int x_index = sizeof(map)/sizeof(map[0]);
	int y_index = sizeof(map[0])/sizeof(map[0][0]);

	double likelihood_field[x_index][y_index];
	double tmp;

	for(int i = 0; i < x_index; i++)
	{
		for(int j = 0; j < y_index; j++)
		{
			likelihood_field[i][j] = DBL_MAX;
			for(int l = 0; l < x_index; l++)
			{
				for(int m = 0; m < y_index; m++)
				{
					if(map[l][m] == 1)
					{
						tmp = scale_factor*(l-i)*scale_factor*(l-i) + 
						      scale_factor*(m-j)*scale_factor*(m-j);
						if(tmp < likelihood_field[i][j])
						{
							likelihood_field[i][j] = tmp;
						}
					}
				}
			}
			printf("%.3f\t",likelihood_field[i][j]);
		}
		printf("\n");
	}
	for(int i = 0; i < x_index; i++)
	{
		for(int j = 0; j < y_index; j++)
		{
			printf("%d\t",map[i][j]);
		}
		printf("\n");
	}
	printf("%d, %d\n", x_index, y_index);

	struct point path[] = {{ 3.        ,  1.        },
	                       { 2.02874018,  1.02677768},
	                       { 1.27043672,  1.29660694},
	                       { 0.87648811,  1.98590974},
	                       { 0.72966178,  2.96961039},
	                       { 0.72090632,  4.05814855},
	                       { 0.86019614,  5.06784558},
	                       { 1.26609607,  5.8250437 },
	                       { 2.04417211,  6.20997552},
	                       { 3.03616153,  6.31839611},
	                       { 4.01420258,  6.20140479},
	                       { 4.75827663,  5.79609341},
	                       { 5.13098922,  5.01941501},
	                       { 5.24848991,  4.01559674},
	                       { 5.21713729,  2.96897628},
	                       { 5.03961037,  2.04864845},
	                       { 4.64199035,  1.38662256},
	                       { 4.        ,  1.        }};
	//struct point path[] = {{0.0,0.0},{0.0,10.0},
	//{10.0,10.0},{10.0,0.0},{0.0,0.0}};
	int n = sizeof(path)/sizeof(path[0]);

	//{{start loc},start_heading,length_of_car}
	struct vehicle car = {{3.0,1.0},PI,0.5}; 

	printf("car location: (%f,%f) car orientation: %f rad\n", 
	       car.location.x, car.location.y, car.orientation);

	double params[] = {2.0,6.0,0.0000}; //{P,I,D}
	pid(path, n, car, params);

	double sigsquare = 2.0;
	double efx = erf(sqrt(2.0)/sqrt(2*sigsquare));
	printf("%.7f",efx);

	return 0;
}
