#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "autonomous_vehicle.h"

struct vehicle move(struct vehicle car, double dist, double steer)
{

				if(steer > MAX_STEER){
								steer = MAX_STEER;
				}
				if(steer < -MAX_STEER){
								steer = -MAX_STEER;
				}
				if(dist < 0.0){
								dist = 0.0;
				}

				// Add a constant systematic bias to test the integral term
				//steer += 0.17;
				double turn = tan(steer)*dist/car.length;
				if( fabs(turn) < 0.001){
								//printf("straight line");
								// straight line model
								car.location.x = car.location.x + (dist * cos(car.orientation));
							  car.location.y = car.location.y + (dist * sin(car.orientation));
                car.orientation = (car.orientation + turn); 
								
								// Normalize it to a 0 to 2*PI circle.
								if (car.orientation < 0.0){
												car.orientation += 2*PI;
								}
								car.orientation -=  floor(car.orientation / (2*PI)) * 2*PI;
				} else {
								// bicycle model
								double radius = dist / turn;
                double cx = car.location.x - (sin(car.orientation) * radius);
                double cy = car.location.y + (cos(car.orientation) * radius);
                car.orientation = (car.orientation + turn);

								// Normalize it to a 0 to 2*PI circle.
								if (car.orientation < 0.0){
												car.orientation += 2*PI;
								}
								car.orientation -=  floor(car.orientation / (2*PI)) * 2*PI;

                car.location.x = cx + (sin(car.orientation) * radius);
                car.location.y = cy - (cos(car.orientation) * radius);
				}

				return car;
}

int pid(struct point path[], int n, struct vehicle car,double params[3])
{
				printf("goal: (%f, %f)\n",path[n-1].x, path[n-1].y);
				double cte = 0.0;
				double diff_cte = 0.0;
				double int_cte = 0.0;
				double dx, dy, rx, ry, normalized_projection; 
				double steer = 0.0;

				// Set up a file for data logging.
				FILE *f = fopen("output.csv","w+");
				if (f == NULL)
				{
								printf("error with file");
								exit(1);
				}

				for( int i =0; i < n - 1; i++ )
				{
								dx = path[i+1].x - path[i].x;
								dy = path[i+1].y - path[i].y;

								/*---------------------------------
								 * Eventually the car needs to sense it's location here,
								 * assigning these values to car.location
								 * ------------------------------*/

							  rx = car.location.x - path[i].x;                                								
								ry = car.location.y - path[i].y;

								normalized_projection = (rx*dx + ry*dy)/((dx*dx) + (dy*dy));

								cte =  (ry*dx - rx*dy)/sqrt((dx*dx) + (dy*dy));
								//cte = car.location.y;


								while(normalized_projection < 1.0)
								{
												diff_cte = -cte;	
												cte =  (ry*dx - rx*dy)/sqrt((dx*dx) + (dy*dy));
												diff_cte += cte;
												int_cte += cte;
												steer = -params[0]*cte - params[1]*diff_cte - params[2]*int_cte;
												
												car = move(car,0.25,steer);

												// Console logging and file logging
												printf("car location: (%f,%f) car orientation: %f rad\n", car.location.x, car.location.y, car.orientation);
												fprintf(f,"%f, %f, %f\n", car.location.x, car.location.y, car.orientation);

												rx = car.location.x - path[i].x;
												ry = car.location.y - path[i].y;

												normalized_projection = (rx*dx + ry*dy)/((dx*dx) + (dy*dy));
								}
				}

				fclose(f);
				return 0;
}

