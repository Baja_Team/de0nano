module state_machine
(
	input statetransition,
	input [3:0] sw,
	input clk,
	input reset,
	output [7:0] led
	
);


typedef enum logic [1:0] {
	A,
	B,
	C,
	OH_SHIT
} state_t;

state_t state, next;

always_ff @(posedge clk) begin // state logic
	if (reset) state<=A;
	else       state<=next;
end

always_comb begin	//next state logic
	case(state)
		A:begin
			if(statetransition) begin
				next=B;
			end
			else begin
				next = A;	
			end
		end
		
		B:begin
			if(statetransition) begin
				next=C;
			end
			else begin
				next = B;	
			end
		end
		C:begin
			if(statetransition) begin
				next=A;
			end
			else begin
				next = C;	
			end
		end

		OH_SHIT: next = OH_SHIT;
	endcase
end

always_ff @(posedge clk) begin // output logic
/*
	case(next)
		A:led[3:0] <= sw;
		B:led[7:4] <= sw;
		C:led[7:0] <= {sw,sw};
		OH_SHIT:led[7:0] <= '1;
	endcase
*/
	case(next)
		A:led <= {4'b000, sw};
		B:led <= {sw,4'b000};
		C:led <= {sw,sw};
		OH_SHIT:led[7:0] <= '1;
	endcase
end

endmodule
