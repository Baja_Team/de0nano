// synthesis VERILOG_INPUT_VERSION SYSTEMVERILOG_2005
`timescale 10ns/10ps

`define CLK_SPEED 50000000

import uart_pkg::*;

module top_level 
(
    input         CLK50MHZ,//do not rename
    input  [1:0]  KEY,
    input  [3:0]  SWITCH,
    /*      DRAM SIGNALS    */
    output [12:0] DRAM_ADDR,
    output [15:0] DRAM_DATA,
    output [1:0]  DRAM_BANK_ADDR,
    output [1:0]  DRAM_DQM, //data byte mask
    output        DRAM_RAS_N,
    output        DRAM_CAS_N,
    output        DRAM_CLK_EN,
    output        DRAM_CLK,
    output        DRAM_WR_EN,
    output        DRAM_CS_N,

    /* EEPROM/GSENSOR SIGNALS  */
    output        EEPROM_SCLK,
    input         EEPROM_SDAT,
    input         G_SENSOR_INT,
    output        G_SENSOR_CS,

    /*        ADC SIGNALS     */
    output        ADC_CS_N, //LOW ACTIVE!
    output        ADC_SADDR,
    input         ADC_SDAT,
    output        ADC_SCLK,

    /*       GPIO Signals by pin Position  */
    input         JP1_1, //must be input
    output        JP1_2,
    input         JP1_3, //must be input
    output        JP1_4,
    output        JP1_5,
    output        JP1_6,
    output        JP1_7,
    output        JP1_8,
    output        JP1_9,
    output        JP1_10,
    //output      JP1_11, //VCC_5V
    //output      JP1_12, //GND
    output        JP1_13,
    output        JP1_14,
    output        JP1_15,
    output        JP1_16,
    output        JP1_17,
    output        JP1_18,
    output        JP1_19,
    output        JP1_20,
    output        JP1_21,
    output        JP1_22,
    output        JP1_23,
    output        JP1_24,
    output        JP1_25,
    output        JP1_26,
    output        JP1_27,
    output        JP1_28,
    //output      JP1_29, //VCC_3v3
    //output      JP1_30, //GND
    output        JP1_31,
    output        JP1_32,
    output        JP1_33,
    output        JP1_34,
    output        JP1_35,
    output        JP1_36,
    output        JP1_37,
    output        JP1_38,
    output        JP1_39,
    output        JP1_40,

    /*       GPIO Signals by pin Position */
    input         JP2_1, //must be input
    output        JP2_2,
    input         JP2_3, //must be input
    output        JP2_4,
    output        JP2_5,
    output        JP2_6,
    output        JP2_7,
    output        JP2_8,
    output        JP2_9,
    output        JP2_10,
    //output      JP2_11, //VCC_5V
    //output      JP2_12, //GND
    output        JP2_13,
    output        JP2_14,
    output        JP2_15,
    output        JP2_16,
    output        JP2_17,
    output        JP2_18,
    output        JP2_19,
    output        JP2_20,
    output        JP2_21,
    output        JP2_22,
    output        JP2_23,
    output        JP2_24,
    output        JP2_25,
    output        JP2_26,
    output        JP2_27,
    output        JP2_28,
    //output      JP2_29, //VCC_3v3
    //output      JP2_30, //GND
    output        JP2_31,
    output        JP2_32,
    output        JP2_33,
    output        JP2_34,
    output        JP2_35,
    output        JP2_36,
    output        JP2_37,
    output        JP2_38,
    output        JP2_39,
    output        JP2_40,

    /*       GPIO Signals by pin Position */
    //input         JP3_1, //VCC 3v3
    input         JP3_2, //must be input
    input         JP3_3, //must be input
    input         JP3_4, //must be input
    output        JP3_5,
    output        JP3_6,
    output        JP3_7,
    output        JP3_8,
    output        JP3_9,
    output        JP3_10,
    output        JP3_11,
    output        JP3_12,
    output        JP3_13,
    output        JP3_15,
    output        JP3_16,
    output        JP3_17,

    output reg [7:0]  LED
);

logic [3:0] led_left ;
wire  [3:0] led_right;

assign led_right = SWITCH;

assign LED = {led_left, led_right};

always_comb begin
	led_left = led_right;
/*
	case (SWITCH)
		4'h1 : led_left = 4'b0001;
		4'h2 : led_left = 4'b0010;
		4'h3 : led_left = 4'b0011;
		4'h4 : led_left = 4'b0100;
		4'h5 : led_left = 4'b0101;
		4'h6 : led_left = 4'b0110;
		4'h7 : led_left = 4'b0111;
		4'h8 : led_left = 4'b1000;
		default: led_left = '1;
	endcase
*/
end

endmodule
