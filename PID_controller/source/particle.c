#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "autonomous_vehicle.h"
const double X_KSENSE[] = {1.0,-1.0};
const double Y_KSENSE[] = {0.0, 0.0};
const double SIG_SQUARED[] = {1.0,1.0};
const double Z_HIT[] = {.5,.5};
const double Z_RAND[] = {.1,.1};
const double Z_MAX[] = {.4,.4};
const double MAX_RANGE[] = {40.0,20.0};

double rand_lim(double limit)
{
/* return a random number between 0 and limit inclusive.
 */

    double divisor = (double)RAND_MAX/(limit);
    double retval;

    do { 
        retval = (double)rand() / divisor;
    } while (retval > limit);

    return retval;
}

/*
 * Carry out particle filter algorithm on a set of N particles.
 * measurement: sensor data actually measured by the vehicle in an 360 element  
 * array.
 * control: a vector of 2 control parameters: distance and steering
 * N: number of particles
 * K: Number of measurements & angles
 */
struct vehicle * particle_filter( 
	struct vehicle * particles,
	int N, 
	double * measurement, 
	double * angles, 
	int * sensor_number,
	int K, 
	double * control,
	double * likelihood_field,
	int rows,
	int cols
	)
{
	struct vehicle particles_temp[N];
	double weights[N];
	double dist = *control;
	double steer = *(control + 1);
	for(int i = 0; i < N; i++)
	{
		//TODO: the probabilistic motion model isn't happening yet. 
		//      Should it for the PF?
		struct vehicle x_t = move(particles[i], dist, steer);
		//TODO: How is scaling information being represented? Right now the 
		//      likelihood field, measurement vector, and sensor locations
		//      are not necessarily scaled to the same units. This needs to be
		//      set consistently in one place.
		double w_particle =  meas_prob( x_t, measurement, angles, sensor_number,
		                                K, likelihood_field, rows, cols);
		particles_temp[i] = x_t;
		weights[i] = w_particle;
	}
	for(int i = 0; i < N; i++)
	{
		particles[i] = draw_particle(particles_temp, weights, N);
	}
	return particles;
}

//TODO: implement particle draw with replacement
struct vehicle draw_particle(struct vehicle * particles, 
                             double * weights, int N)
{
	printf("N: %d, %f", N, weights[0]);
	return particles[0];
}

/*
 * Calculate the probability of a given measurement vector from a particle's 
 * position. Uses a Likelihood field method. Likelihood field must be passed in.
 * K: The length of the measurement vector
 *
 * Refer to Sebastian Thrun's book: Probabilistic Robotics P. 143 on Likelihood
 * Fields
 */
double meas_prob(
	struct vehicle particle,
	double * measurements,
	double * angles, 
	int * sensor_number,
	int K, 
	double * likelihood_field, 
	int dimx, 
	int dimy
	)
{
	double q = 1;
	double x_ksense,y_ksense,variance,z_hit,z_rand,z_max, max_range, dist_sq;
	int x_z, y_z;
	for(int i = 0; i < K; i++)
	{
		x_ksense = X_KSENSE[sensor_number[i]];
		y_ksense = Y_KSENSE[sensor_number[i]];
		variance = SIG_SQUARED[sensor_number[i]];
		z_hit    = Z_HIT[sensor_number[i]];
		z_rand   = Z_RAND[sensor_number[i]];
		z_max    = Z_MAX[sensor_number[i]];
		max_range = MAX_RANGE[sensor_number[i]];

		if(measurements[i] <= max_range)
		{
			//TODO: ensure that the floor is the right thing to do here in 
			//      all cases
			//TODO: Scaling on x_z and y_z
			x_z = floor(
			            particle.location.x + 
			            x_ksense*cos(particle.orientation) -
			            y_ksense*sin(particle.orientation) + 
			            measurements[i]*cos(particle.orientation + angles[i])
			           );

			y_z = floor(
			            particle.location.y + 
			            y_ksense*cos(particle.orientation) +
			            x_ksense*sin(particle.orientation) + 
			            measurements[i]*sin(particle.orientation + angles[i])
			           );
		}
		//TODO: scaling of dist_sq? or should the LF come in scaled?
		dist_sq = likelihood_field[x_z*dimy + y_z];
		//TODO: implement the intrinsic parameter choice algorithm for sigma,
		//      z_hit, etc page 129 of probabilistic robotics

		// Ensure that the desired cell is in the grid
		if(x_z >= 0 && x_z < dimx && y_z >=0 && y_z < dimy)
		{
			q = q*(z_hit*erf(sqrt(dist_sq/sqrt(2*variance)) + z_rand/z_max));
		} else {
			// This treatment is from Probabilistic Robotics p.144
			// on unexplored regions of the map
			q = q*(1/z_max);
		}
	}
	return q;
}

