`timescale 10ns/10ps
module ultrasonic #(parameter TIMEOUT=50000000,  //in clocks
                    parameter PULSE_LENGTH=50000,//in clocks
                    parameter TIMER_WIDTH=35     //# of bits (needs to be greater than log2(CLK_SPEED)
                   )
(
    input clk,
    input reset,
    input echo,        //result back from the ultrasonic sensor
    input auto_trig,   //set to 1 to auto_trigger
    input single_trig, //set to 1 to trigger once
    output trig,
    output [7:0] distance
);

//****** GENERAL GUIDELINES ********
//1. every always_ff should have a reset condition that resets all
//   variables set in that always block
//2. try to use names instead of hard coded values. much easier to read :)
//3. try to imagine the hardware created from each always block
//4. every always block should have a simple and clear purpose. 
//   try not to blend them into one (try to treat them like functions in C)
//   aka they shouldn't be all that complicated and should only do one simple
//   thing.
//5. I have made a solution that we can go over tomorrow. Best of luck! :)

//registers needed go here

//simple logic checks go here

//state names go here
enum logic [1:0]{
    IDLE,
    SEND_TRIG,
    MEASURE
} state, next;

always_ff @(posedge clk) begin //timer logic
    //timer logic goes here
end

always_ff @(posedge clk) begin //state logic
    //state transition logic goes here
end

always_ff @(posedge clk) begin //output logic
    //output logic goes here
end

always_comb begin //next state logic
    //next state logic goes here (use simple logic statements from above)
end

endmodule
