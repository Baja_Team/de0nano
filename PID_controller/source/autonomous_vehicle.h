#ifndef AUTONOMOUS_VEHICLE
#define AUTONOMOUS_VEHICLE
#define PI 3.14159265

struct point
{
				double x;
				double y;
};

struct vehicle
{
				struct point location;
				double orientation;
				double length;
};
#define MAX_STEER PI/4.0

struct vehicle move(struct vehicle car, double dist, double steer);

int pid(struct point path[], int n, struct vehicle car,double params[3]);

double rand_lim(double limit);

struct vehicle * particle_filter(
	struct vehicle * particles,
	int N, 
	double * measurement, 
	double * angles, 
	int * sensor_number,
	int K, 
	double * control,
	double * likelihood_field,
	int rows,
	int cols
	);

struct vehicle draw_particle(struct vehicle * particles, double * weights, int N);

//struct vehicle * particle(int N, struct vehicle start_loc, int x_max, int y_max, struct vehicle * particles);

double meas_prob(
	struct vehicle particle,
	double * measurements,
	double * angles, 
	int * sensor_number,
	int K, 
	double * likelihood_field, 
	int dimx, 
	int dimy
	);
#endif
